const path = require('path');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

module.exports = {
    mode: 'development',

    context: path.join(__dirname, 'src'),

    entry: {
        home: './Home',
        order: './Order',
        profile: './Profile',
        shop: './Shop'
    },

    output: {
        path: path.join(__dirname, 'built'),
        filename: '[name].js'
    },

    resolve: {
        extensions: ['.ts'],
        plugins: [
            new TsconfigPathsPlugin()
        ]
    },

    module: {
        rules: [{
            test: /\.ts$/,
            loader: 'ts-loader'
        }]
    },

    devtool: 'none'
}