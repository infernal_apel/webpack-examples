const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
    mode: 'production',

    context: path.join(__dirname, 'src'),

    entry: {
        a: './a',
        b: './b'
    },

    output: {
        path: path.join(__dirname, 'built'),
        filename: '[name].js'
    },

    resolve: {
        extensions: ['.js']
    },

    optimization: {
        minimize: true,
        minimizer: [
            // The plugin is explicitly added just for example, it is used by default if production mode is true
            // https://webpack.js.org/configuration/optimization/#optimizationminimize
            new TerserPlugin({
                cache: true,
                parallel: true,
                sourceMap: true, // Must be set to true if using source-maps in production
                terserOptions: {
                    // https://github.com/webpack-contrib/terser-webpack-plugin#terseroptions
                }
            })
        ]
    },

    devtool: 'source-map'
}