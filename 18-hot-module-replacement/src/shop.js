import * as app from './components/application';
import * as item from './components/item';

window.addEventListener('load', function () {
    console.log('Page has been refreshed!');
});

app.action();

console.log('You are on the Shop Page');

item.action();

if (module.hot) {
    module.hot.accept('./components/application', function () {
        console.log('Accepting the updated app module!');
        app.action();
    });
}