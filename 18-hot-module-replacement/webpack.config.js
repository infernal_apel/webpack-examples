const path = require('path');
const {HotModuleReplacementPlugin} = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {

    mode: 'development',

    context: path.join(__dirname, 'src'),

    entry: {
        shop: './shop'
    },

    output: {
        path: path.join(__dirname, 'built'),
        filename: '[name].js'
    },

    module: {
        rules: [{
            test: /\.css$/,
            use: ['style-loader', 'css-loader']
        }]
    },

    plugins: [
        new HtmlWebpackPlugin({
            template: './index.html'
        }),
        new HotModuleReplacementPlugin()
    ],

    devServer: {
        hot: true,
        port: 4200,
        open: true
    },

    devtool: 'sourcemap'
};