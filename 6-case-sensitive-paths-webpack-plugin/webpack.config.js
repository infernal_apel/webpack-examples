const path = require('path');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');

module.exports = {
    mode: 'development',

    entry: './src/init.js',

    output: {
        path: path.join(__dirname, 'built'),
        filename: 'index_bundle.js',
    },

    plugins: [
        new CaseSensitivePathsPlugin()
    ]
}