import logo from './images/logo.png';

const app = document.getElementById('app');

app.innerHTML = `
    <img
        src="${logo}"
        alt="webpack logo"
    />
`;