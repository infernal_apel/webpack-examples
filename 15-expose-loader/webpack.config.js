const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    mode: 'development',

    context: path.join(__dirname, 'src'),

    entry: {
        vendor: ['lodash'],
        a: './a'
    },

    output: {
        path: path.join(__dirname, 'built'),
        filename: '[name].js'
    },

    module: {
        rules: [{
            test: require.resolve('lodash'),
            loader: 'expose-loader?lodash'
        }]
    },

    resolve: {
        extensions: ['.js']
    },

    plugins: [
        new HtmlWebpackPlugin({
            template: './index.html'
        })
    ],

    devtool: 'none'
}