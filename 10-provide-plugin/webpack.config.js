const path = require('path');
const webpack = require('webpack');

module.exports = {
    mode: 'development',

    entry: './main.js',

    output: {
        path: path.join(__dirname, 'built'),
        filename: '[name].js'
    },

    resolve: {
        extensions: ['.js']
    },

    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery'
        })
    ],

    devtool: 'none'
}