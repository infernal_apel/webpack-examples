const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

// https://webpack.js.org/plugins/mini-css-extract-plugin/
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    mode: 'development',

    context: path.join(__dirname, 'src'),

    entry: {
        home: './init',
        styles: './home.css',
        one: './one.css'
    },

    output: {
        path: path.join(__dirname, 'built'),
        filename: '[name].js'
    },

    resolve: {
        extensions: ['.js']
    },

    module: {
        rules: [{
            test: /\.css$/,
            use: ['style-loader', 'css-loader'],
            // use: [MiniCssExtractPlugin.loader, 'css-loader']
        }]
    },

    plugins: [
        new HtmlWebpackPlugin({
            template: './index.html'
        }),
        new MiniCssExtractPlugin()
    ],

    devtool: 'none'
}