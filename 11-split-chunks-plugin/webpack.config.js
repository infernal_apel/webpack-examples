const path = require('path');

module.exports = {
    mode: 'production',

    context: path.join(__dirname, 'src'),

    entry: {
        home: './Home',
        order: './Order',
        profile: './Profile',
        shop: './Shop'
    },

    output: {
        path: path.join(__dirname, 'built'),
        filename: '[name].js'
    },

    resolve: {
        extensions: ['.js']
    },

    optimization: {
        // https://webpack.js.org/plugins/split-chunks-plugin/#optimizationsplitchunks
        splitChunks: {
            cacheGroups: {
                vendor: {
                    name: 'vendor',
                    chunks: 'initial',
                    minChunks: 2
                }
            }
        }
    },

    devtool: 'source-map'
}